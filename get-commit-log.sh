#!/bin/bash -x

# Prepare commit numbers from git logs
# The output is intended for further data manipulation as to produce a CSV using a python script.

RAW_GROUP=$1
SINCE=$2
UNTIL=$3
GIT_PROVIDER=$4

if [[ -z ${RAW_GROUP} || -z ${SINCE} || -z ${UNTIL} || -z ${GIT_PROVIDER} ]]; then
  echo "this script requires three positional arguments (org, since, until and gitprovider)"
  echo "gitprovider is either github or gitlab"
  exit 1
fi

if [[ "${GIT_PROVIDER}" == "gitlab" ]]; then
  BASE_API_URL="https://gitlab.ow2.org/api/v4"
  read -p 'provide gitlab personal token: ' GITLAB_TOKEN

  # First check the group name
  _R=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${BASE_API_URL}/groups/${RAW_GROUP}?with_projects=no")

  _ERR=$(echo ${_R} | jq -r .error)
  if [[ "${_ERR}" != "null" ]]; then # there is a valid error value
    # could it be an expired token ?
    echo "An error occured: exit (${_ERR})"
    exit 1
  fi

  GROUP=$(echo ${_R} | jq -r .name)
  if [[ "${GROUP}" == "null" ]]; then
    echo "this Group/Org was not found"
    exit 1
  fi

  # We have the "real" group name, instead of the user-given group which is case insensitive
  unset RAW_GROUP

  echo "Gather repos URLs for this group (100 max)"
  REPO_LIST=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${BASE_API_URL}/groups/${GROUP}/projects?simple=yes&per_page=100" | jq -r .[].web_url)

  # group folder are stored in the same folder regardless the git provider, need to differentiate
  GROUP="${GROUP}.gitlab"

elif [[ "${GIT_PROVIDER}" == "github" ]]; then
  BASE_API_URL="https://api.github.com"
  # First check the group name
  GROUP=$(curl -s ${BASE_API_URL}/orgs/${RAW_GROUP} | jq -r .login)

  if [[ "${GROUP}" == "null" ]]; then
    echo "this Group/Org was not found"
    exit 1
  else
    # We have the "real" group name, instead of the user-given group which is case insensitive
    unset RAW_GROUP
    GROUP_REPOS_URL=$(curl -s ${BASE_API_URL}/orgs/${GROUP} | jq -r .repos_url)

    echo "Gather repo URLs for this org (100 max)"
    REPO_LIST=$(curl -s "${GROUP_REPOS_URL}?type=sources&per_page=100" | jq -r .[].html_url)

  fi


else
  echo "unknown git provider "${GIT_PROVIDER}" (should be either gitlab or github)"
  exit 1
fi



OUTPUT_DIR="stats/${GROUP}"
echo "cleaning stats folder ${OUTPUT_DIR} if exists"

if [[ -d ${OUTPUT_DIR} ]]; then
  rm -rf ${OUTPUT_DIR} && mkdir ${OUTPUT_DIR}
else
  mkdir ${OUTPUT_DIR}
fi

echo "will clone repos (only if repo clone folder does not exist yet)"
for REPO in ${REPO_LIST}; do
  # extract repo name from URL (last component)
  REPO_NAME=${REPO##*/}

  # clone repo
  [[ -d /data/git-stats/${GROUP} ]] || mkdir /data/git-stats/${GROUP}

  if [[ -d /data/git-stats/${GROUP}/${REPO_NAME} ]]; then
    echo "not cloning (dir already exists)"
  else
    echo "cloning ${REPO}..."

    if [[ "${GIT_PROVIDER}" == "gitlab" ]]; then
      REPO_AUTH=${REPO/https:\/\//https:\/\/oauth2:${GITLAB_TOKEN}@}
    else
      REPO_AUTH=${REPO}
    fi

    git -C /data/git-stats/${GROUP} clone --quiet ${REPO_AUTH}
  fi

  [[ -d ${OUTPUT_DIR} ]] || mkdir ${OUTPUT_DIR}

  # we store the log timeframe somewhere so the python script can generate the CSV header properly
  echo "${SINCE} ${UNTIL}" > ${OUTPUT_DIR}.timeframe

  # first line is the repo full URL, so we can reference it in the CSV
  echo ${REPO} > ${OUTPUT_DIR}/${REPO_NAME}.commits

  # extract and append commits data
  echo "extracting commit logs for ${REPO_NAME} / default branch ($(git -C /data/git-stats/${GROUP}/${REPO_NAME} branch --show-current))"
  git -C /data/git-stats/${GROUP}/${REPO_NAME} log \
  --since=${SINCE} \
  --until=${UNTIL} \
  --no-merges \
  --pretty="format:%cd" \
  --date=format:'%Y-%m' | uniq -c|sort -k 2 >> ${OUTPUT_DIR}/${REPO_NAME}.commits

  echo
done

echo "Now you can compute the data with the python script to produce the CSV"
