#!/usr/bin/python3
# coding: utf8

from os import scandir
import glob
import csv
import datetime
from dateutil.relativedelta import relativedelta


def record_csv(projects, group):
    with open(f'{group}-commits.csv', 'w', newline='') as csvfile:
        fieldnames = gen_fieldnames(group)
        commit_writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        commit_writer.writeheader()
        for p_commit_data in projects:
            # when there is no commit on that month, we fill with value 0
            for f in fieldnames:
                if f not in p_commit_data:
                    p_commit_data[f] = 0
            commit_writer.writerow(p_commit_data)


def gen_fieldnames(group):
    '''generate fields name for the CSV header'''

    with open(f'{group}.timeframe', 'r') as periodfile:
        start_repr, stop_repr = periodfile.readline().split()

    dt = datetime.datetime.strptime(start_repr, '%Y-%m-%d')
    dstop = datetime.datetime.strptime(stop_repr, '%Y-%m-%d')
    step = relativedelta(months=1)

    fields = ['project']  # first column contains the project name

    # append remaining fields for the period
    while dt < dstop:
        fields.append(dt.strftime('%Y-%m'))
        dt += step
        pass

    return fields


if __name__ == '__main__':
    for entry in scandir('stats/'):
        if entry.is_dir():
            group = entry.path
            projects = []
            for commit_file in glob.iglob(f'{group}/*.commits'):
                commit_data = {}
                # this is a single project in the org
                with open(commit_file, 'r') as commit_file:
                    repo_url = commit_file.readline().splitlines()[0]  # first line is the repo URL
                    for entry in commit_file:  # remaining lines are commit values
                        commit_number, commit_month = entry.split()
                        commit_number = int(commit_number)
                        commit_data[commit_month] = commit_number

                commit_data['project'] = repo_url
                # sort commit_data by month
                commit_data = {k: commit_data[k] for k in sorted(commit_data)}

                projects.append(commit_data)

            record_csv(projects, group)
