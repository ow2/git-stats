# Statistics from Git repos

## Commits Statistics
These scripts help to generate a CSV file containing the number of commits per month from a given GitHub org and time frame as input.

![CSV Example](stats-table.png)

## Usage
There are two scripts. They are intended to be sequentially:
1. [get-commit-log.sh](get-commit-log.sh) : gather repos URLs from given organisation (group), clone repos and extract commit logs (from current default branch) within the time frame.
2. [compute-data.py](compute-data.py) : takes the data from the shell script above and generates a CSV.

## Needed improvements
- Implement GitLab support in the shell script, so as to retrieve repos URL for a given group.

## Possible improvements
- It doesn't gather commits from all branch (but in which case(s) would it be useful ?)
- Repositories clones are not updated from remote when restarting the shell script, mainly because for now I'm not sure how to handle the possible case where the default branch happen to change.
